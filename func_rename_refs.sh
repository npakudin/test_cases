#!/bin/bash
set -x #show commands
#set -e #exit on error

items=$1
cmd_try=$2
cmd_on_success_upd=$3
cmd_on_success_cur_1=$4
cmd_on_success_cur_2=$5

for item in $items
do
	# replace FooBarBaz with Foo_Bar_Baz and then with foo_bar_baz
	low_item=$(echo $item | sed -e "s/\([a-z]\)\([A-Z]\)/\1_\2/g" | tr [A-Z] [a-z])
	echo $item

	if [ $item == $low_item ]; then
		echo "$low_item: name is OK, skip"
	else
		for suffix in "" _1 _2 _3 _4 _5 _6 _7 _8 _9 _10
		do
		  git checkout $item
			upd_item="$low_item$suffix"

			$cmd_try $upd_item # git tag $upd_item
			ret_val=$?
			if [ $ret_val -eq 0 ]; then
				# OK
				$cmd_on_success_upd $upd_item # git push origin -u
				$cmd_on_success_cur_1 $item # git push --delete origin $item
				$cmd_on_success_cur_2 $item # git tag --delete $item
				echo "$item renamed to $upd_item"
				break
			else
				echo "Failed renaming $item to $upd_item"
			fi
		done
	fi
done
