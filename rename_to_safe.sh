#!/bin/bash
set -x #show commands
#set -e #exit on error

# allow to call func_* from any folder
cmd_name=$0
cmd_path=$(echo "$cmd_name" | sed -e 's#\(.*/\)[^/]*$#\1#')

git fetch
git checkout master

# branches
items=$("$cmd_path"/func_get_remote_branches.sh)
"$cmd_path"/func_rename_refs.sh "$items" "git branch -m" "git push origin -u" "git push origin --delete" "echo"

# tags
items=$(git tag -l)
"$cmd_path"/func_rename_refs.sh "$items" "git tag" "echo" "git push --delete origin" "git tag --delete"
