#!/bin/bash
set -x #show commands
#set -e #exit on error

git branch -a | grep -v "remotes/origin/" | cut -d" " -f 3 | grep -v HEAD | grep -v master | sed -e "s#remotes/origin/##g" | sort
