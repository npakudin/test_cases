At the GIT server for the server hook

Once: place global repo with update hook

    cd /opt
    sudo mkdir git_settings
    cd git_settings
    sudo git clone https://bitbucket.org/npakudin/test_cases.git
     
In the each repo: set symlink to that update file
If this file already exists - you have to investigate by yourself

    cd .git/hooks
    ln -s /opt/git_settings/test_cases/update update 


At the Jenkins server for branch renaming:

Once: place global repo with script for renaming

    cd /opt
    sudo mkdir git_settings
    cd git_settings
    sudo git clone https://bitbucket.org/npakudin/test_cases.git

In the each repo: clone & run script

    git clone git@bitbucket.org:npakudin/test_cases.git --depth 1 --no-single-branch
    cd test_cases
    /opt/git_settings/test_cases/rename_to_safe.sh >res.txt 2>res.txt
    grep "renamed to" res.txt # to see renamed refs
    
