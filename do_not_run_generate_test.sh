#!/bin/bash
set -x #show commands
#set -e #exit on error

# allow to call func_* from any folder
cmd_name=$0
cmd_path=$(echo "$cmd_name" | sed -e 's#\(.*/\)[^/]*$#\1#')

git fetch
git checkout master


echo "remove remote and local branches"
for item in $("$cmd_path"/func_get_remote_branches.sh)
do
    git push origin --delete $item
done

for item in $("$cmd_path"/func_get_local_branches.sh)
do
    git branch -D $item
done



echo "remove remote and local tags"
for item in $(git tag -l | sort)
do
    git push --delete origin $item
    git tag --delete $item
done



echo "create branches"
for branch in Feature/FooBarBaz Feature/FooBarBaz_2 feature/FooBarBaz feature/foo_bar_baz
do
	echo "Create branch $branch"
	git checkout -b $branch
	git push --set-upstream origin $branch
done


echo "create tags"
for tag in Tag/FooBarBaz Tag/FooBarBaz_2 tag/FooBarBaz tag/foo_bar_baz
do
  echo "Create tag $tag"
  git tag $tag
done

git push --tags

